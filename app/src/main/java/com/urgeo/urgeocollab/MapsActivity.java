package com.urgeo.urgeocollab;

import androidx.annotation.DrawableRes;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.urgeo.urgeocollab.constant.Constant;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private static final String TAG = "MAPS_ACTIVITY";
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.style2_json));

            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }

        LatLng position = new LatLng(Constant.DEFAUT_MAP_X, Constant.DEFAULT_MAP_Y);
        LatLng pap1 = new LatLng(18.5790242,-72.3548438);
        LatLng pap2 = new LatLng(18.7402101,-72.2180747);
        LatLng pap3 = new LatLng(18.4550601,-72.1692097);
        mMap.addMarker(new MarkerOptions().position(pap1).icon(BitmapDescriptorFactory.fromResource(R.drawable.electric)).title("Essai Electrique"));
        mMap.addMarker(new MarkerOptions().position(pap2).icon(BitmapDescriptorFactory.fromResource(R.drawable.seism)).title("Essai Sismique"));
        mMap.addMarker(new MarkerOptions().position(pap3).icon(BitmapDescriptorFactory.fromResource(R.drawable.forage)).title("Forage"));
        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(position, 7);

        mMap.animateCamera(location);
    }

}
