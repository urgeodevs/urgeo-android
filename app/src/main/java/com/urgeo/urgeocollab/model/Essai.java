package com.urgeo.urgeocollab.model;

import android.graphics.Point;

import lombok.Data;
import java.io.Serializable;
import java.util.Date;

@Data
public class Essai implements Serializable {
    private Long id;
    private String altId;
    private String siteName;
    private Point geometryPoint;
    private Date essaiDateTime;
    private String file;
    private String image;
    private double depth;
    private double velocity;
    private double sectionPointe;
    private long nbCoup;
    private double rsDynamicPointe;
    private double depthRefus;
    private double resistance;
    private String comment;
    private String note;
    private Date dateCreated;
    private String createdBy;
    private String modifiedBy;
    private Date dateModified;
}
