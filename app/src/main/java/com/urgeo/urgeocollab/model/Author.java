package com.urgeo.urgeocollab.model;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class Author implements Serializable {
    private Long id;
    private String firtsname;
    private String lastname;
    private String tels;
    private String email;
    private Date dateCreated;
    private String createdBy;
    private String modifiedBy;
    private Date dateModified;
}
