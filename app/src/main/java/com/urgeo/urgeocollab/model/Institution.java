package com.urgeo.urgeocollab.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class Institution {
    private Long id;
    private String name;
    private String desc;
    private String details;
    private String tels;
    private String email;
    private String address;
    private Date dateCreated;
    private String createdBy;
    private String modifiedBy;
    private Date dateModified;
}
