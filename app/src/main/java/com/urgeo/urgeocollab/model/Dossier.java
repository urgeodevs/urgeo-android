package com.urgeo.urgeocollab.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class Dossier {
    private Long id;
    private String code;
    private String desc;
    private Date dateCreated;
    private String createdBy;
    private String modifiedBy;
    private Date dateModified;
}
